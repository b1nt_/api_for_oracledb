package com.service.useoperations;

import com.service.api.customtypes.ComplexId;

/**
 * Created by Слава on 23.12.2016.
 */
public class UseUtil {
    static Integer parseStringToIntId(String stringId) {
        Integer intId = null;
        try {
                intId = Integer.parseInt(stringId);
        } catch (Exception ex) {
            System.err.println("Error! Failed to parse id to integer.\n" + ex.toString());
            System.exit(2);
        }
        return intId;
    }

    static ComplexId parseStringToComplexId(String id1, String id2) {
        ComplexId complexId = null;
        try {
            complexId = new ComplexId(Integer.parseInt(id1), Integer.parseInt(id2));
        } catch (Exception ex) {
            System.err.println("Error! Failed to parse workroom_id or equipment_id to integer.\n" + ex.toString());
            System.exit(2);
        }
        return complexId;
    }
}
