package com.service.useoperations;

import com.service.api.operationswithdb.WorkroomOperation;
import com.service.api.entity.WorkroomEntity;

import java.util.List;

/**
 * Created by Слава on 23.12.2016.
 */
public class UseWorkroom extends UseTable{
    WorkroomOperation workroomOperation;

    public UseWorkroom() {
        workroomOperation = new WorkroomOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 2) {
            WorkroomEntity workroomEntity = null;
            try {
                workroomEntity = new WorkroomEntity(Integer.parseInt(params[0]), params[1], Integer.parseInt(params[2]));
            } catch (Exception ex) {
                System.err.println("Error! Failed to create workroomEntity.\n" + ex.toString());
                return;
            }

            Integer result = workroomOperation.create(workroomEntity);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<WorkroomEntity> we = workroomOperation.read(id);
        if(we != null) {
            for (WorkroomEntity we1 : we)
                System.out.println(we1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 2) {
            WorkroomEntity workroomEntity = null;
            Integer id = null;

            try {
                workroomEntity = new WorkroomEntity(Integer.parseInt(params[0]), params[1], Integer.parseInt(params[2]));
                if(params.length > 3) {
                    id = Integer.parseInt(params[3]);
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed to create workroomEntity or id not integer.\n" + ex.toString());
                return;
            }

            Integer result = workroomOperation.update(workroomEntity, id);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

         Integer result = workroomOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
