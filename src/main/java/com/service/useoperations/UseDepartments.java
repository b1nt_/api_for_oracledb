package com.service.useoperations;

import com.service.api.operationswithdb.DepartmentsOperation;
import com.service.api.entity.DepartmentsEntity;

import java.util.List;

/**
 * Created by b1nt_ on 23.12.2016.
 */
public class UseDepartments extends UseTable{
    private DepartmentsOperation departmentsOperation;

    public UseDepartments() {
        departmentsOperation = new DepartmentsOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 1) {
            DepartmentsEntity departmentsEntity = null;

            try {
                 departmentsEntity = new DepartmentsEntity(params[0], Integer.parseInt(params[1]));
            } catch (Exception ex) {
                System.err.println("Error! Failed to create departmentsEntity.\n" + ex.toString());
                return;
            }

            Integer result = departmentsOperation.create(departmentsEntity);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<DepartmentsEntity> dp = departmentsOperation.read(id);
        if (dp != null) {
            for (DepartmentsEntity dp1 : dp)
                System.out.println(dp1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 1) {
            DepartmentsEntity departmentsEntity = null;
            Integer id = null;

            try {
                departmentsEntity = new DepartmentsEntity(params[0], Integer.parseInt(params[1]));
                if(params.length > 2) {
                    id = Integer.parseInt(params[2]);
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed to create departmentsEntity or id not integer.\n" + ex.toString());
                return;
            }

            Integer result = departmentsOperation.update(departmentsEntity, id);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        Integer result = departmentsOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
