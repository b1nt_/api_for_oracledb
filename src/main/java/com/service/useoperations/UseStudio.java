package com.service.useoperations;

import com.service.api.operationswithdb.StudioOperation;
import com.service.api.entity.StudioEntity;

import java.util.List;

/**
 * Created by b1nt_ on 23.12.2016.
 */
public class UseStudio extends UseTable{
    private StudioOperation studioOperation;

    public UseStudio() {
        studioOperation = new StudioOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 1) {
            StudioEntity studioEntity = new StudioEntity(params[0], params[1]);

            Integer result = studioOperation.create(studioEntity);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<StudioEntity> se = studioOperation.read(id);
        if(se != null) {
            for (StudioEntity se1 : se)
                System.out.println(se1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 1) {
            StudioEntity studioEntity = new StudioEntity(params[0], params[1]);
            Integer id = null;

            if(params.length > 2) {
                id = UseUtil.parseStringToIntId(params[2]);
            }

            Integer result = studioOperation.update(studioEntity, id);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        Integer result = studioOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
