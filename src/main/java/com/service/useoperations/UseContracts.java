package com.service.useoperations;

import com.service.api.operationswithdb.ContractsOperation;
import com.service.api.entity.ContractsEntity;

import java.sql.Date;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UseContracts extends UseTable{
    private ContractsOperation contractsOperation;

    public UseContracts() {
        contractsOperation = new ContractsOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 4) {
            ContractsEntity contractsEntity = null;

            try {
                contractsEntity = new ContractsEntity(params[0],
                        Date.valueOf(params[1]),
                        Date.valueOf(params[2]),
                        Float.parseFloat(params[3]),
                        params[4]);
            } catch (Exception ex) {
                System.err.println("Error! Failed to create contractsEntity.\n" + ex.toString());
                return;
            }

            Integer result = contractsOperation.create(contractsEntity);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<ContractsEntity> ce = contractsOperation.read(id);
        if (ce != null) {
            for (ContractsEntity ce1 : ce)
                System.out.println(ce1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 4) {
            ContractsEntity contractsEntity = null;
            Integer id = null;

            try {
                contractsEntity = new ContractsEntity(params[0],
                        Date.valueOf(params[1]),
                        Date.valueOf(params[2]),
                        Float.parseFloat(params[3]),
                        params[4]);
                if(params.length > 5) {
                    id = Integer.parseInt(params[5]);
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed to create contractsEntity or id not integer.\n" + ex.toString());
                return;
            }

            Integer result = contractsOperation.update(contractsEntity, id);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        Integer result = contractsOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
