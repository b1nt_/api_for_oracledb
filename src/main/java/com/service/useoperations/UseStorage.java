package com.service.useoperations;

import com.service.api.operationswithdb.StorageOperation;
import com.service.api.entity.StorageEntity;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UseStorage extends UseTable{
    private StorageOperation storageOperation;

    public UseStorage() {
        storageOperation = new StorageOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 2) {
            StorageEntity storageEntity = null;
            FileInputStream input = null;

            try {
                File file = new File(params[2]);
                input = new FileInputStream(file);
                storageEntity = new StorageEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
            } catch (Exception ex) {
                System.err.println("Error! Failed to create storageEntity.\n" + ex.toString());
                return;
            }

            Integer result = storageOperation.create(storageEntity, input);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<StorageEntity> dp = storageOperation.read(id);
        if (dp != null) {
            for (StorageEntity dp1 : dp)
                System.out.println(dp1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 3) {
            StorageEntity storageEntity = null;
            Integer id = null;
            FileInputStream input = null;

            try {
                File file = new File(params[2]);
                input = new FileInputStream(file);
                storageEntity = new StorageEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
                if(params.length > 3) {
                    id = Integer.parseInt(params[3]);
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed to create storageEntity or id not integer.\n" + ex.toString());
                return;
            }

            Integer result = storageOperation.update(storageEntity, id, input);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        Integer result = storageOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
