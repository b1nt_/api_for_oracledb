package com.service.useoperations;

import com.service.api.operationswithdb.WorkroomInventoryOperation;
import com.service.api.entity.WorkroomInventoryEntity;
import com.service.api.customtypes.ComplexId;

import java.util.List;

/**
 * Created by Слава on 23.12.2016.
 */
public class UseWorkroomInventory extends UseTable{
    private WorkroomInventoryOperation workroomInventoryOperation;

    public UseWorkroomInventory() {
        workroomInventoryOperation = new WorkroomInventoryOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 1) {
            WorkroomInventoryEntity workroomInventoryEntity = null;
            try {
                workroomInventoryEntity = new WorkroomInventoryEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create workroomInventoryEntity.\n" + ex.toString());
                return;
            }

            ComplexId complexId = workroomInventoryOperation.create(workroomInventoryEntity);
            System.out.println("Return: " + complexId);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        List<WorkroomInventoryEntity> wie = workroomInventoryOperation.read(complexId);
        if(wie != null) {
            for (WorkroomInventoryEntity wie1 : wie)
                System.out.println(wie1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 1) {
            ComplexId complexId = null;
            WorkroomInventoryEntity workroomInventoryEntity = null;

            try {
                workroomInventoryEntity = new WorkroomInventoryEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
                if (params.length > 3) {
                    complexId = new ComplexId(Integer.parseInt(params[2]), Integer.parseInt(params[3]));
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create workroomInventoryEntity or to parse workroom_id or equipment_id to integer.\n" + ex.toString());
                return;
            }

            Integer result = workroomInventoryOperation.update(workroomInventoryEntity, complexId);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        Integer result = workroomInventoryOperation.delete(complexId);
        System.out.println("Return: " + result);
    }
}
