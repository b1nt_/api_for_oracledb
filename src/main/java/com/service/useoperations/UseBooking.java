package com.service.useoperations;

import com.service.api.operationswithdb.BookingOperation;
import com.service.api.entity.BookingEntity;
import com.service.api.customtypes.ComplexId;

import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UseBooking extends UseTable{
    private BookingOperation bookingOperation;

    public UseBooking() {
        bookingOperation = new BookingOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 1) {
            BookingEntity bookingEntity = null;
            try {
                bookingEntity = new BookingEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create bookingEntity.\n" + ex.toString());
                return;
            }

            ComplexId complexId = bookingOperation.create(bookingEntity);
            System.out.println("Return: " + complexId);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        List<BookingEntity> wie = bookingOperation.read(complexId);
        if(wie != null) {
            for (BookingEntity wie1 : wie)
                System.out.println(wie1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 1) {
            ComplexId complexId = null;
            BookingEntity bookingEntity = null;

            try {
                bookingEntity = new BookingEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
                if (params.length > 3) {
                    complexId = new ComplexId(Integer.parseInt(params[2]), Integer.parseInt(params[3]));
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create bookingEntity or to parse contract_id or department_id to integer.\n" + ex.toString());
                return;
            }

            Integer result = bookingOperation.update(bookingEntity, complexId);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        Integer result = bookingOperation.delete(complexId);
        System.out.println("Return: " + result);
    }
}
