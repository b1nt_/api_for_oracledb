package com.service.useoperations;

import com.service.api.operationswithdb.AlbumInfoOperation;
import com.service.api.entity.AlbumInfoEntity;
import com.service.api.customtypes.ComplexId;

import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UseAlbumInfo extends UseTable {
    private AlbumInfoOperation albumInfoOperation;

    public UseAlbumInfo() {
        albumInfoOperation = new AlbumInfoOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 1) {
            AlbumInfoEntity albumInfoEntity = null;
            try {
                albumInfoEntity = new AlbumInfoEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create albumInfoEntity.\n" + ex.toString());
                return;
            }

            ComplexId complexId = albumInfoOperation.create(albumInfoEntity);
            System.out.println("Return: " + complexId);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        List<AlbumInfoEntity> sie = albumInfoOperation.read(complexId);
        if(sie != null) {
            for (AlbumInfoEntity sie1 : sie)
                System.out.println(sie1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 1) {
            ComplexId complexId = null;
            AlbumInfoEntity albumInfoEntity = null;

            try {
                albumInfoEntity = new AlbumInfoEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
                if (params.length > 3) {
                    complexId = new ComplexId(Integer.parseInt(params[2]), Integer.parseInt(params[3]));
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create albumInfoEntity or to parse song_id or person_id to integer.\n" + ex.toString());
                return;
            }

            Integer result = albumInfoOperation.update(albumInfoEntity, complexId);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        Integer result = albumInfoOperation.delete(complexId);
        System.out.println("Return: " + result);
    }
}
