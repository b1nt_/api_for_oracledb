package com.service.useoperations;

/**
 * Created by Слава on 26.12.2016.
 */
public abstract class UseTable {
    public abstract void useCreate(String[] params);
    public abstract void useRead(String[] params);
    public abstract void useUpdate(String[] params);
    public abstract void useDelete(String[] params);
}
