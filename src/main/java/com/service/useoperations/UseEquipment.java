package com.service.useoperations;

import com.service.api.operationswithdb.EquipmentOperation;
import com.service.api.entity.EquipmentEntity;

import java.util.List;

/**
 * Created by Слава on 23.12.2016.
 */
public class UseEquipment extends UseTable{
    private EquipmentOperation equipmentOperation;

    public UseEquipment() {
        equipmentOperation = new EquipmentOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 0) {
            EquipmentEntity equipmentEntity = new EquipmentEntity(params[0]);

            Integer result = equipmentOperation.create(equipmentEntity);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<EquipmentEntity> ee = equipmentOperation.read(id);
        if(ee != null) {
            for (EquipmentEntity ee1 : ee)
                System.out.println(ee1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 0) {
            EquipmentEntity equipmentEntity = new EquipmentEntity(params[0]);
            Integer id = null;

            if(params.length > 1) {
                id = UseUtil.parseStringToIntId(params[1]);
            }

            Integer result = equipmentOperation.update(equipmentEntity, id);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        Integer result = equipmentOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
