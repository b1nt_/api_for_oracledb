package com.service.useoperations;

import com.service.api.operationswithdb.SongsInfoOperation;
import com.service.api.entity.SongsInfoEntity;
import com.service.api.customtypes.ComplexId;

import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UseSongsInfo extends UseTable{
    private SongsInfoOperation songsInfoOperation;

    public UseSongsInfo() {
        songsInfoOperation = new SongsInfoOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 1) {
            SongsInfoEntity songsInfoEntity = null;
            try {
                songsInfoEntity = new SongsInfoEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create songsInfoEntity.\n" + ex.toString());
                return;
            }

            ComplexId complexId = songsInfoOperation.create(songsInfoEntity);
            System.out.println("Return: " + complexId);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        List<SongsInfoEntity> sie = songsInfoOperation.read(complexId);
        if(sie != null) {
            for (SongsInfoEntity sie1 : sie)
                System.out.println(sie1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 1) {
            ComplexId complexId = null;
            SongsInfoEntity songsInfoEntity = null;

            try {
                songsInfoEntity = new SongsInfoEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
                if (params.length > 3) {
                    complexId = new ComplexId(Integer.parseInt(params[2]), Integer.parseInt(params[3]));
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create songsInfoEntity or to parse song_id or person_id to integer.\n" + ex.toString());
                return;
            }

            Integer result = songsInfoOperation.update(songsInfoEntity, complexId);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        Integer result = songsInfoOperation.delete(complexId);
        System.out.println("Return: " + result);
    }
}
