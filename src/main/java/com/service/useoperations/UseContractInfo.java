package com.service.useoperations;

import com.service.api.operationswithdb.ContractInfoOperation;
import com.service.api.entity.ContractInfoEntity;
import com.service.api.customtypes.ComplexId;

import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UseContractInfo extends UseTable{
    private ContractInfoOperation contractInfoOperation;

    public UseContractInfo() {
        contractInfoOperation = new ContractInfoOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 1) {
            ContractInfoEntity contractInfoEntity = null;
            try {
                contractInfoEntity = new ContractInfoEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create contractInfoEntity.\n" + ex.toString());
                return;
            }

            ComplexId complexId = contractInfoOperation.create(contractInfoEntity);
            System.out.println("Return: " + complexId);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        List<ContractInfoEntity> cie = contractInfoOperation.read(complexId);
        if(cie != null) {
            for (ContractInfoEntity cie1 : cie)
                System.out.println(cie1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 1) {
            ComplexId complexId = null;
            ContractInfoEntity contractInfoEntity = null;

            try {
                contractInfoEntity = new ContractInfoEntity(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
                if (params.length > 3) {
                    complexId = new ComplexId(Integer.parseInt(params[2]), Integer.parseInt(params[3]));
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create contractInfoEntity or to parse contract_id or person_id to integer.\n" + ex.toString());
                return;
            }

            Integer result = contractInfoOperation.update(contractInfoEntity, complexId);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        ComplexId complexId = null;

        if(params.length > 1) {
            complexId = UseUtil.parseStringToComplexId(params[0], params[1]);
        }

        Integer result = contractInfoOperation.delete(complexId);
        System.out.println("Return: " + result);
    }
}
