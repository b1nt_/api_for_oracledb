package com.service.useoperations;

import com.service.api.operationswithdb.SongsOperation;
import com.service.api.entity.SongsEntity;

import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UseSongs extends UseTable{
    private SongsOperation songsOperation;

    public UseSongs() {
        songsOperation = new SongsOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 0) {
            SongsEntity songsEntity = new SongsEntity(params[0]);

            Integer result = songsOperation.create(songsEntity);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<SongsEntity> se = songsOperation.read(id);
        if(se != null) {
            for (SongsEntity se1 : se)
                System.out.println(se1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 0) {
            SongsEntity songsEntity = new SongsEntity(params[0]);
            Integer id = null;

            if(params.length > 1) {
                id = UseUtil.parseStringToIntId(params[1]);
            }

            Integer result = songsOperation.update(songsEntity, id);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        Integer result = songsOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
