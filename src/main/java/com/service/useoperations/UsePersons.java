package com.service.useoperations;

import com.service.api.operationswithdb.PersonsOperation;
import com.service.api.entity.PersonsEntity;
import com.service.api.customtypes.Person;

import java.sql.Date;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UsePersons extends UseTable{
    private PersonsOperation personsOperation;

    public UsePersons() {
        personsOperation = new PersonsOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 5) {
            PersonsEntity personsEntity = null;

            try {
                personsEntity = new PersonsEntity(new Person(params[0], params[1], params[2], Date.valueOf(params[3])), params[4], Integer.parseInt(params[5]));
            } catch (Exception ex) {
                System.err.println("Error! Failed to create personsEntity.\n" + ex.toString());
                return;
            }

            Integer result = personsOperation.create(personsEntity);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<PersonsEntity> pe = personsOperation.read(id);
        if (pe != null) {
            for (PersonsEntity pe1 : pe)
                System.out.println(pe1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 5) {
            PersonsEntity personsEntity = null;
            Integer id = null;

            try {
                personsEntity = new PersonsEntity(new Person(params[0], params[1], params[2], Date.valueOf(params[3])), params[4], Integer.parseInt(params[5]));
                if(params.length > 6) {
                    id = Integer.parseInt(params[6]);
                }
            } catch (Exception ex) {
                System.err.println("Error! Failed to create personsEntity or id not integer.\n" + ex.toString());
                return;
            }

            Integer result = personsOperation.update(personsEntity, id);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        Integer result = personsOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
