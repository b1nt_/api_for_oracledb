package com.service.useoperations;

import com.service.api.operationswithdb.SoundtrackTypeOperation;
import com.service.api.entity.SoundtrackTypeEntity;

import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UseSoundtrackType extends UseTable{
    private SoundtrackTypeOperation soundtrackTypeOperation;

    public UseSoundtrackType() {
        soundtrackTypeOperation = new SoundtrackTypeOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 0) {
            SoundtrackTypeEntity soundtrackTypeEntity = new SoundtrackTypeEntity(params[0]);

            Integer result = soundtrackTypeOperation.create(soundtrackTypeEntity);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<SoundtrackTypeEntity> ee = soundtrackTypeOperation.read(id);
        if(ee != null) {
            for (SoundtrackTypeEntity ee1 : ee)
                System.out.println(ee1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 0) {
            SoundtrackTypeEntity soundtrackTypeEntity = new SoundtrackTypeEntity(params[0]);
            Integer id = null;

            if(params.length > 1) {
                id = UseUtil.parseStringToIntId(params[1]);
            }

            Integer result = soundtrackTypeOperation.update(soundtrackTypeEntity, id);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        Integer result = soundtrackTypeOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
