package com.service.useoperations;

import com.service.api.operationswithdb.AlbumsOperation;
import com.service.api.entity.AlbumsEntity;

import java.sql.Date;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class UseAlbums extends UseTable{
    private AlbumsOperation albumsOperation;

    public UseAlbums() {
        albumsOperation = new AlbumsOperation();
    }

    public void useCreate(String[] params) {
        if(params.length > 2) {
            AlbumsEntity albumsEntity = null;

            try {
                albumsEntity = new AlbumsEntity(params[0], Date.valueOf(params[1]), Integer.parseInt(params[2]));
            } catch (Exception ex) {
                System.err.println("Error! Failed  to create AlbumsEntity.\n" + ex.toString());
                return;
            }

            Integer result = albumsOperation.create(albumsEntity);
            System.out.println("Return: " + result);
        }
        else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useRead(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        List<AlbumsEntity> ae = albumsOperation.read(id);
        if(ae != null) {
            for (AlbumsEntity ae1 : ae)
                System.out.println(ae1);
        }
    }

    public void useUpdate(String[] params) {
        if(params.length > 2) {
            AlbumsEntity albumsEntity = null;
            Integer id = null;

            try {
                albumsEntity = new AlbumsEntity(params[0], Date.valueOf(params[1]), Integer.parseInt(params[2]));
            } catch (Exception ex) {
                System.err.println("Error! Failed to create AlbumsEntity.\n" + ex.toString());
                return;
            }

            if(params.length > 3) {
                id = UseUtil.parseStringToIntId(params[3]);
            }

            Integer result = albumsOperation.update(albumsEntity, id);
            System.out.println("Return: " + result);
        } else {
            System.err.println("Error! Wrong number of arguments.");
        }
    }

    public void useDelete(String[] params) {
        Integer id = null;

        if(params.length > 0) {
            id = UseUtil.parseStringToIntId(params[0]);
        }

        Integer result = albumsOperation.delete(id);
        System.out.println("Return: " + result);
    }
}
