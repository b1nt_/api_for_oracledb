package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Locale;

/**
 * Created by Слава on 15.12.2016.
 */
public class DataBaseUtil {
    private static Connection connection = null;
    private final String url = "jdbc:oracle:thin:@localhost:1521:XE";
    private final String name = "system";
    private final String password = "root";
    //private final String driverName = "oracle.jdbc.driver.OracleDriver";

    public Connection createConnection () {
        try {
            //Class.forName(driverName);
            Locale.setDefault(Locale.ENGLISH);
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            connection = DriverManager.getConnection(url, name, password);
        } catch (Exception ex) {
            System.err.println("Faild to create connection." + ex);
            System.exit(1);
        }
        return connection;
    }

    public static Connection getConnection() {
        return connection;
    }
}
