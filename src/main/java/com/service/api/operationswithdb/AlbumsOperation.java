package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.AlbumsEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class AlbumsOperation {
    public Integer create(AlbumsEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO ALBUMS (name, release_date, quantity) VALUES (?, ?, ?)", new String[] {"album_id"});
            preparedStatement.setString(1,entity.getName());
            preparedStatement.setDate(2, entity.getRelease_date());
            preparedStatement.setInt(3, entity.getQuantity());

            preparedStatement.executeUpdate();
            if (preparedStatement.getGeneratedKeys().next()) {
                return preparedStatement.getGeneratedKeys().getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Error in AlbumsOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<AlbumsEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM ALBUMS ";

            if(id != null) {
                query += "WHERE album_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            List<AlbumsEntity> albumEntityList = new ArrayList<>();
            while (resultSet.next()) {
                albumEntityList.add(new AlbumsEntity(
                        resultSet.getInt("album_id"),
                        resultSet.getString("name"),
                        resultSet.getDate("release_date"),
                        resultSet.getInt("quantity")));
            }

            return albumEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in AlbumsOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(AlbumsEntity entity, Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE ALBUMS SET name = ?, release_date = ?, quantity = ?";

            if(id != null) {
                query += "WHERE album_id = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getName());
                preparedStatement.setDate(2, entity.getRelease_date());
                preparedStatement.setInt(3, entity.getQuantity());
                preparedStatement.setInt(4, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getName());
                preparedStatement.setDate(2, entity.getRelease_date());
                preparedStatement.setInt(3, entity.getQuantity());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in AlbumsOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM ALBUMS";

            if(id != null) {
                query +=  " WHERE album_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in AlbumsOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
