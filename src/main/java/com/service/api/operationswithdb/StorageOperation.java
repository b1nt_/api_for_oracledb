package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.StorageEntity;

import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class StorageOperation {
    public Integer create(StorageEntity entity, FileInputStream input) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO STORAGE (song_id, type, soundtrack) VALUES (?, ?, ?)");
            preparedStatement.setInt(1,entity.getSond_id());
            preparedStatement.setInt(2, entity.getType());
            preparedStatement.setBinaryStream(3, input);

            Integer result = preparedStatement.executeUpdate();
            return result;
        } catch (SQLException ex) {
            System.err.println("Error in StorageOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<StorageEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM STORAGE ";

            if(id != null) {
                query += "WHERE song_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            List<StorageEntity> storageEntityList = new ArrayList<StorageEntity>();
            while (resultSet.next()) {
                storageEntityList.add(new StorageEntity(
                        resultSet.getInt("song_id"),
                        resultSet.getInt("type"),
                        resultSet.getBlob("soundtrack")));
            }

            return storageEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in StorageOperationeration.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(StorageEntity entity, Integer id, FileInputStream input) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE STORAGE SET song_id = ?, type = ?, soundtrack = ?";

            if(id != null) {
                query += "WHERE song_id = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,entity.getSond_id());
                preparedStatement.setInt(2, entity.getType());
                preparedStatement.setBinaryStream(3, input);
                preparedStatement.setInt(4, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,entity.getSond_id());
                preparedStatement.setInt(2, entity.getType());
                preparedStatement.setBinaryStream(3, input);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in StorageOperationeration.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM STORAGE";

            if(id != null) {
                query +=  " WHERE song_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in StorageOperation.read() \n" + ex.toString());
        }
        return 0;
    }

}
