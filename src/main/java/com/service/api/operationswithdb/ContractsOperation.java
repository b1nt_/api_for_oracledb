package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.ContractsEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class ContractsOperation {
    public Integer create(ContractsEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO CONTRACTS (name, start_date, end_date, cost, description) VALUES (?, ?, ?, ?, ?)", new String[] {"contract_id"});
            preparedStatement.setString(1,entity.getName());
            preparedStatement.setDate(2,entity.getStart_date());
            preparedStatement.setDate(3,entity.getEnd_date());
            preparedStatement.setFloat(4,entity.getCost());
            preparedStatement.setString(5, entity.getDescription());

            preparedStatement.executeUpdate();
            if (preparedStatement.getGeneratedKeys().next()) {
                return preparedStatement.getGeneratedKeys().getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Error in ContractsOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<ContractsEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM CONTRACTS ";

            if(id != null) {
                query += "WHERE contract_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            List<ContractsEntity> contractEntityList = new ArrayList<ContractsEntity>();
            while (resultSet.next()) {
                contractEntityList.add(new ContractsEntity(
                        resultSet.getInt("contract_id"),
                        resultSet.getString("name"),
                        resultSet.getDate("start_date"),
                        resultSet.getDate("end_date"),
                        resultSet.getFloat("cost"),
                        resultSet.getString("description")));
            }

            return contractEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in ContractsOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(ContractsEntity entity, Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE CONTRACTS SET name = ?, start_date = ?, end_date = ?, cost = ?, description = ?";

            if(id != null) {
                query += "WHERE contract_id = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getName());
                preparedStatement.setDate(2,entity.getStart_date());
                preparedStatement.setDate(3,entity.getEnd_date());
                preparedStatement.setFloat(4,entity.getCost());
                preparedStatement.setString(5,entity.getDescription());
                preparedStatement.setInt(6, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getName());
                preparedStatement.setDate(2,entity.getStart_date());
                preparedStatement.setDate(3,entity.getEnd_date());
                preparedStatement.setFloat(4,entity.getCost());
                preparedStatement.setString(5,entity.getDescription());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in ContractsOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM CONTRACTS";

            if(id != null) {
                query +=  " WHERE contract_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in ContractsOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
