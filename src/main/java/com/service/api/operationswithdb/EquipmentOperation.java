package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.EquipmentEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 23.12.2016.
 */
public class EquipmentOperation {
    public Integer create(EquipmentEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO EQUIPMENT (name) VALUES (?)", new String[] {"equipment_id"});
            preparedStatement.setString(1,entity.getName());

            preparedStatement.executeUpdate();
            if (preparedStatement.getGeneratedKeys().next()) {
                return preparedStatement.getGeneratedKeys().getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Error in EquipmentOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<EquipmentEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM EQUIPMENT ";

            if(id != null) {
                query += "WHERE equipment_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            List<EquipmentEntity> equipmentEntityList = new ArrayList<EquipmentEntity>();
            while (resultSet.next()) {
                equipmentEntityList.add(new EquipmentEntity(
                        resultSet.getInt("equipment_id"),
                        resultSet.getString("name")));
            }

            return equipmentEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in EquipmentOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(EquipmentEntity entity, Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE EQUIPMENT SET name = ?";

            if(id != null) {
                query += "WHERE equipment_id = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1, entity.getName());
                preparedStatement.setInt(2, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1, entity.getName());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in EquipmentOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM EQUIPMENT";

            if(id != null) {
                query +=  " WHERE equipment_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in EquipmentOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
