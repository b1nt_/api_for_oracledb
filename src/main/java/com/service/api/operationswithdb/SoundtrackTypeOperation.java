package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.SoundtrackTypeEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class SoundtrackTypeOperation {
    public Integer create(SoundtrackTypeEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO SOUNDTRACKTYPE (description) VALUES (?)", new String[] {"type"});
            preparedStatement.setString(1,entity.getDescription());

            preparedStatement.executeUpdate();
            if (preparedStatement.getGeneratedKeys().next()) {
                return preparedStatement.getGeneratedKeys().getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Error in SoundtrackTypeOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<SoundtrackTypeEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM SOUNDTRACKTYPE ";

            if(id != null) {
                query += "WHERE type = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            List<SoundtrackTypeEntity> equipmentEntityList = new ArrayList<SoundtrackTypeEntity>();
            while (resultSet.next()) {
                equipmentEntityList.add(new SoundtrackTypeEntity(
                        resultSet.getInt("type"),
                        resultSet.getString("description")));
            }

            return equipmentEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in SoundtrackTypeOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(SoundtrackTypeEntity entity, Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE SOUNDTRACKTYPE SET description = ?";

            if(id != null) {
                query += "WHERE type = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1, entity.getDescription());
                preparedStatement.setInt(2, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1, entity.getDescription());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in SoundtrackTypeOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM SOUNDTRACKTYPE";

            if(id != null) {
                query +=  " WHERE type = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in SoundtrackTypeOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
