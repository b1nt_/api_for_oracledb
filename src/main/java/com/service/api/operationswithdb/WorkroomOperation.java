package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.WorkroomEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 20.12.2016.
 */
public class WorkroomOperation {
    public Integer create(WorkroomEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO WORKROOM (room_number, name, studio_id) VALUES (?, ?, ?)", new String[] {"workroom_id"});
            preparedStatement.setInt(1,entity.getRoom_number());
            preparedStatement.setString(2,entity.getName());
            preparedStatement.setInt(3, entity.getStudio_id());

            preparedStatement.executeUpdate();
            if (preparedStatement.getGeneratedKeys().next()) {
                return preparedStatement.getGeneratedKeys().getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Error in WorkroomOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<WorkroomEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM WORKROOM ";

            if(id != null) {
                query += "WHERE workroom_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            List<WorkroomEntity> workroomEntityList = new ArrayList<WorkroomEntity>();
            while (resultSet.next()) {
                workroomEntityList.add(new WorkroomEntity(
                        resultSet.getInt("workroom_id"),
                        resultSet.getInt("room_number"),
                        resultSet.getString("name"),
                        resultSet.getInt("studio_id")));
            }

            return workroomEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in WorkroomOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(WorkroomEntity entity, Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE WORKROOM SET room_number = ? , name = ?, studio_id = ?";

            if(id != null) {
                query += "WHERE workroom_id = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,entity.getRoom_number());
                preparedStatement.setString(2,entity.getName());
                preparedStatement.setInt(3,entity.getStudio_id());
                preparedStatement.setInt(4, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,entity.getRoom_number());
                preparedStatement.setString(2,entity.getName());
                preparedStatement.setInt(3,entity.getStudio_id());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in WorkroomOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM WORKROOM";

            if(id != null) {
                query +=  " WHERE workroom_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in WorkroomOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
