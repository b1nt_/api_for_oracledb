package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.DepartmentsEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 15.12.2016.
 */
public class DepartmentsOperation {
    public Integer create(DepartmentsEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO DEPARTMENTS (name, studio_id) VALUES (?, ?)", new String[] {"department_id"});
            preparedStatement.setString(1,entity.getName());
            preparedStatement.setInt(2, entity.getStudio_id());

            preparedStatement.executeUpdate();
            if (preparedStatement.getGeneratedKeys().next()) {
                return preparedStatement.getGeneratedKeys().getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Error in DepartmentsOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<DepartmentsEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM DEPARTMENTS ";

            if(id != null) {
                query += "WHERE department_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            List<DepartmentsEntity> departmentsEntityList = new ArrayList<DepartmentsEntity>();
            while (resultSet.next()) {
                departmentsEntityList.add(new DepartmentsEntity(
                        resultSet.getInt("department_id"),
                        resultSet.getString("name"),
                        resultSet.getInt("studio_id")));
            }

            return departmentsEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in DepartmentsOperationeration.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(DepartmentsEntity entity, Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE DEPARTMENTS SET name = ?, studio_id = ?";

            if(id != null) {
                query += "WHERE department_id = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getName());
                preparedStatement.setInt(2,entity.getStudio_id());
                preparedStatement.setInt(3, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getName());
                preparedStatement.setInt(2,entity.getStudio_id());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in DepartmentsOperationeration.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM DEPARTMENTS";

            if(id != null) {
                query +=  " WHERE department_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in DepartmentsOperation.read() \n" + ex.toString());
        }
        return 0;
    }
}
