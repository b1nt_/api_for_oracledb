package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.SongsEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class SongsOperation {
    public Integer create(SongsEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO SONGS (name) VALUES (?)", new String[] {"song_id"});
            preparedStatement.setString(1,entity.getName());

            preparedStatement.executeUpdate();
            if (preparedStatement.getGeneratedKeys().next()) {
                return preparedStatement.getGeneratedKeys().getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Error in SongsOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<SongsEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM SONGS ";

            if(id != null) {
                query += "WHERE song_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            List<SongsEntity> equipmentEntityList = new ArrayList<SongsEntity>();
            while (resultSet.next()) {
                equipmentEntityList.add(new SongsEntity(
                        resultSet.getInt("song_id"),
                        resultSet.getString("name")));
            }

            return equipmentEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in SongsOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(SongsEntity entity, Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE SONGS SET name = ?";

            if(id != null) {
                query += "WHERE song_id = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1, entity.getName());
                preparedStatement.setInt(2, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1, entity.getName());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in SongsOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM SONGS";

            if(id != null) {
                query +=  " WHERE song_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in SongsOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
