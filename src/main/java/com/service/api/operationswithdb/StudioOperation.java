package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.StudioEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 20.12.2016.
 */
public class StudioOperation {
    public Integer create(StudioEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO STUDIO (name, address) VALUES (?, ?)", new String[] {"studio_id"});
            preparedStatement.setString(1,entity.getName());
            preparedStatement.setString(2, entity.getAddress());

            preparedStatement.executeUpdate();
            if (preparedStatement.getGeneratedKeys().next()) {
                return preparedStatement.getGeneratedKeys().getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Error in StudioOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<StudioEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM STUDIO ";

            if(id != null) {
                query += "WHERE studio_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            List<StudioEntity> studioEntityList = new ArrayList<StudioEntity>();
            while (resultSet.next()) {
                studioEntityList.add(new StudioEntity(
                        resultSet.getInt("studio_id"),
                        resultSet.getString("name"),
                        resultSet.getString("address")));
            }

            return studioEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in StudioOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(StudioEntity entity, Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE STUDIO SET name = ?, address = ?";

            if(id != null) {
                query += "WHERE studio_id = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getName());
                preparedStatement.setString(2,entity.getAddress());
                preparedStatement.setInt(3, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getName());
                preparedStatement.setString(2,entity.getAddress());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in StudioOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM STUDIO";

            if(id != null) {
                query +=  " WHERE studio_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in StudioOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
