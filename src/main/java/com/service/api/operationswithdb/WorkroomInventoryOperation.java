package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.WorkroomInventoryEntity;
import com.service.api.customtypes.ComplexId;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by b1nt_ on 23.12.2016.
 */
public class WorkroomInventoryOperation {
    public ComplexId create(WorkroomInventoryEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO WORKROOMINVENTORY (workroom_id, equipment_id) VALUES (?, ?)");
            preparedStatement.setInt(1,entity.getWorkroom_id());
            preparedStatement.setInt(2, entity.getEquipment_id());

            int result = preparedStatement.executeUpdate();
            if (result != 0) {
                return new ComplexId(entity.getWorkroom_id(), entity.getEquipment_id());
            }
        } catch (SQLException ex) {
            System.err.println("Error in WorkroomInventoryOperation.create() \n" + ex.toString());
        }
        return null;
    }

    public List<WorkroomInventoryEntity> read(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM WORKROOMINVENTORY";

            if(complexId != null) {
                query += " WHERE workroom_id = ? AND equipment_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            List<WorkroomInventoryEntity> workroomInventoryEntityList = new ArrayList<WorkroomInventoryEntity>();
            while (resultSet.next()) {
                workroomInventoryEntityList.add(new WorkroomInventoryEntity(
                        resultSet.getInt("workroom_id"),
                        resultSet.getInt("equipment_id")));
            }

            return workroomInventoryEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in WorkroomInventoryOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(WorkroomInventoryEntity entity, ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE WORKROOMINVENTORY SET workroom_id = ?, equipment_id = ?";

            if(complexId != null) {
                query += " WHERE workroom_id = ? AND equipment_id";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getWorkroom_id());
                preparedStatement.setInt(2, entity.getEquipment_id());
                preparedStatement.setInt(3, complexId.getId1());
                preparedStatement.setInt(4, complexId.getId2());
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getWorkroom_id());
                preparedStatement.setInt(2, entity.getEquipment_id());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in WorkroomInventoryOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM WORKROOMINVENTORY";

            if(complexId != null) {
                query +=  " WHERE workroom_id = ? AND equipment_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in WorkroomInventoryOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
