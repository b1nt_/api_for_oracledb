package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.AlbumInfoEntity;
import com.service.api.customtypes.ComplexId;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class AlbumInfoOperation {
    public ComplexId create(AlbumInfoEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO ALBUMINFO (album_id, song_id) VALUES (?, ?)");
            preparedStatement.setInt(1,entity.getAlbum_id());
            preparedStatement.setInt(2, entity.getSong_id());

            int result = preparedStatement.executeUpdate();
            if (result != 0) {
                return new ComplexId(entity.getAlbum_id(), entity.getSong_id());
            }
        } catch (SQLException ex) {
            System.err.println("Error in AlbumInfoOperation.create() \n" + ex.toString());
        }
        return null;
    }

    public List<AlbumInfoEntity> read(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM ALBUMINFO";

            if(complexId != null) {
                query += " WHERE album_id = ? AND song_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            List<AlbumInfoEntity> bookingEntityList = new ArrayList<AlbumInfoEntity>();
            while (resultSet.next()) {
                bookingEntityList.add(new AlbumInfoEntity(
                        resultSet.getInt("album_id"),
                        resultSet.getInt("song_id")));
            }

            return bookingEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in AlbumInfoOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(AlbumInfoEntity entity, ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE ALBUMINFO SET album_id = ?, song_id = ?";

            if(complexId != null) {
                query += " WHERE album_id = ? AND song_id";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getAlbum_id());
                preparedStatement.setInt(2, entity.getSong_id());
                preparedStatement.setInt(3, complexId.getId1());
                preparedStatement.setInt(4, complexId.getId2());
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getAlbum_id());
                preparedStatement.setInt(2, entity.getSong_id());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in AlbumInfoOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM ALBUMINFO";

            if(complexId != null) {
                query +=  " WHERE album_id = ? AND song_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in AlbumInfoOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
