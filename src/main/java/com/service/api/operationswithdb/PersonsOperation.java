package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.PersonsEntity;
import com.service.api.customtypes.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class PersonsOperation {
    public Integer create(PersonsEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO PERSONS (person_info, profession, in_staff) VALUES (new person(?, ?, ?, ?), ?, ?)", new String[] {"person_id"});

                    //"INSERT INTO PERSONS (person_info, profession, in_staff) VALUES (new person(?, ?, ?, ?), ?, ?)", new String[] {"person_id"});
            // insert into persons (person_info, profession, in_staff) values (new person('name', 'surname', 'middle_name', to_date('08.09.1996', 'dd.mm.yyyy')), 'proff',1);
            preparedStatement.setString(1,entity.getPerson_ifno().getName());
            preparedStatement.setString(2, entity.getPerson_ifno().getSurname());
            preparedStatement.setString(3, entity.getPerson_ifno().getMiddle_name());
            preparedStatement.setDate(4, entity.getPerson_ifno().getBirthday());
            preparedStatement.setString(5, entity.getProfession());
            preparedStatement.setInt(6, entity.getIn_staff());

            preparedStatement.executeUpdate();
            if (preparedStatement.getGeneratedKeys().next()) {
                return preparedStatement.getGeneratedKeys().getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Error in PersonsOperation.create() \n" + ex.toString());
        }
        return 0;
    }

    public List<PersonsEntity> read(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM PERSONS ";

            if(id != null) {
                query += "WHERE person_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            List<PersonsEntity> studioEntityList = new ArrayList<PersonsEntity>();
            Struct pers;
            while (resultSet.next()) {
                pers = (Struct) resultSet.getObject("person_info");
                Object[] trValues = pers.getAttributes();

                studioEntityList.add(new PersonsEntity(
                        resultSet.getInt("person_id"),
                        new Person((String) trValues[0], (String) trValues[1], (String) trValues[2], new Date(((Timestamp) trValues[3]).getTime()) ),
                        resultSet.getString("profession"),
                        resultSet.getInt("in_staff")));
            }

            return studioEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in PersonsOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(PersonsEntity entity, Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE PERSONS SET person_info = new person(?, ?, ?, ?), profession = ?, in_staff = ?";

            if(id != null) {
                query += "WHERE person_id = ?";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getPerson_ifno().getName());
                preparedStatement.setString(2, entity.getPerson_ifno().getSurname());
                preparedStatement.setString(3, entity.getPerson_ifno().getMiddle_name());
                preparedStatement.setDate(4, entity.getPerson_ifno().getBirthday());
                preparedStatement.setString(5, entity.getProfession());
                preparedStatement.setInt(6, entity.getIn_staff());
                preparedStatement.setInt(7, id);
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setString(1,entity.getPerson_ifno().getName());
                preparedStatement.setString(2, entity.getPerson_ifno().getSurname());
                preparedStatement.setString(3, entity.getPerson_ifno().getMiddle_name());
                preparedStatement.setDate(4, entity.getPerson_ifno().getBirthday());
                preparedStatement.setString(5, entity.getProfession());
                preparedStatement.setInt(6, entity.getIn_staff());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in PersonsOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(Integer id) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM PERSONS";

            if(id != null) {
                query +=  " WHERE person_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, id);
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in PersonsOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
