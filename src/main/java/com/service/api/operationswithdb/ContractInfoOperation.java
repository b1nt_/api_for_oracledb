package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.ContractInfoEntity;
import com.service.api.customtypes.ComplexId;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class ContractInfoOperation {
    public ComplexId create(ContractInfoEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO CONTRACTINFO (contract_id, person_id) VALUES (?, ?)");
            preparedStatement.setInt(1,entity.getContract_id());
            preparedStatement.setInt(2, entity.getPerson_id());

            int result = preparedStatement.executeUpdate();
            if (result != 0) {
                return new ComplexId(entity.getContract_id(), entity.getPerson_id());
            }
        } catch (SQLException ex) {
            System.err.println("Error in ContractInfoOperation.create() \n" + ex.toString());
        }
        return null;
    }

    public List<ContractInfoEntity> read(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM CONTRACTINFO";

            if(complexId != null) {
                query += " WHERE contract_id = ? AND person_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            List<ContractInfoEntity> bookingEntityList = new ArrayList<ContractInfoEntity>();
            while (resultSet.next()) {
                bookingEntityList.add(new ContractInfoEntity(
                        resultSet.getInt("contract_id"),
                        resultSet.getInt("person_id")));
            }

            return bookingEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in ContractInfoOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(ContractInfoEntity entity, ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE CONTRACTINFO SET contract_id = ?, person_id = ?";

            if(complexId != null) {
                query += " WHERE contract_id = ? AND person_id";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getContract_id());
                preparedStatement.setInt(2, entity.getPerson_id());
                preparedStatement.setInt(3, complexId.getId1());
                preparedStatement.setInt(4, complexId.getId2());
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getContract_id());
                preparedStatement.setInt(2, entity.getPerson_id());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in ContractInfoOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM CONTRACTINFO";

            if(complexId != null) {
                query +=  " WHERE contract_id = ? AND person_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in ContractInfoOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
