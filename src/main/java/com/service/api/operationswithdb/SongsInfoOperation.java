package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.SongsInfoEntity;
import com.service.api.customtypes.ComplexId;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class SongsInfoOperation {
    public ComplexId create(SongsInfoEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO SONGSINFO (song_id, person_id) VALUES (?, ?)");
            preparedStatement.setInt(1,entity.getSong_id());
            preparedStatement.setInt(2, entity.getPerson_id());

            int result = preparedStatement.executeUpdate();
            if (result != 0) {
                return new ComplexId(entity.getSong_id(), entity.getPerson_id());
            }
        } catch (SQLException ex) {
            System.err.println("Error in SongsInfoOperation.create() \n" + ex.toString());
        }
        return null;
    }

    public List<SongsInfoEntity> read(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM SONGSINFO";

            if(complexId != null) {
                query += " WHERE song_id = ? AND person_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            List<SongsInfoEntity> bookingEntityList = new ArrayList<SongsInfoEntity>();
            while (resultSet.next()) {
                bookingEntityList.add(new SongsInfoEntity(
                        resultSet.getInt("song_id"),
                        resultSet.getInt("person_id")));
            }

            return bookingEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in SongsInfoOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(SongsInfoEntity entity, ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE SONGSINFO SET song_id = ?, person_id = ?";

            if(complexId != null) {
                query += " WHERE song_id = ? AND person_id";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getSong_id());
                preparedStatement.setInt(2, entity.getPerson_id());
                preparedStatement.setInt(3, complexId.getId1());
                preparedStatement.setInt(4, complexId.getId2());
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getSong_id());
                preparedStatement.setInt(2, entity.getPerson_id());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in SongsInfoOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM SONGSINFO";

            if(complexId != null) {
                query +=  " WHERE song_id = ? AND person_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in SongsInfoOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
