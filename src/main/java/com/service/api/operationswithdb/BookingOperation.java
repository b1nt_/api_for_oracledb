package com.service.api.operationswithdb;

import com.service.DataBaseUtil;
import com.service.api.entity.BookingEntity;
import com.service.api.customtypes.ComplexId;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Слава on 24.12.2016.
 */
public class BookingOperation {
    public ComplexId create(BookingEntity entity) {
        try {
            PreparedStatement preparedStatement = DataBaseUtil.getConnection().prepareStatement(
                    "INSERT INTO BOOKING (contract_id, department_id) VALUES (?, ?)");
            preparedStatement.setInt(1,entity.getContract_id());
            preparedStatement.setInt(2, entity.getDepartment_id());

            int result = preparedStatement.executeUpdate();
            if (result != 0) {
                return new ComplexId(entity.getContract_id(), entity.getDepartment_id());
            }
        } catch (SQLException ex) {
            System.err.println("Error in BookingOperation.create() \n" + ex.toString());
        }
        return null;
    }

    public List<BookingEntity> read(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "SELECT * FROM BOOKING";

            if(complexId != null) {
                query += " WHERE contract_id = ? AND department_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1,complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            List<BookingEntity> bookingEntityList = new ArrayList<BookingEntity>();
            while (resultSet.next()) {
                bookingEntityList.add(new BookingEntity(
                        resultSet.getInt("contract_id"),
                        resultSet.getInt("department_id")));
            }

            return bookingEntityList;
        } catch (SQLException ex) {
            System.err.println("Error in BookingOperation.read() \n" + ex.toString());
        }
        return null;
    }

    public Integer update(BookingEntity entity, ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "UPDATE BOOKING SET contract_id = ?, department_id = ?";

            if(complexId != null) {
                query += " WHERE contract_id = ? AND department_id";
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getContract_id());
                preparedStatement.setInt(2, entity.getDepartment_id());
                preparedStatement.setInt(3, complexId.getId1());
                preparedStatement.setInt(4, complexId.getId2());
            }
            else {
                preparedStatement = DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, entity.getContract_id());
                preparedStatement.setInt(2, entity.getDepartment_id());
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in BookingOperation.update() \n" + ex.toString());
        }
        return 0;
    }

    public Integer delete(ComplexId complexId) {
        try {
            PreparedStatement preparedStatement;
            String query = "DELETE FROM BOOKING";

            if(complexId != null) {
                query +=  " WHERE contract_id = ? AND department_id = ?";
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
                preparedStatement.setInt(1, complexId.getId1());
                preparedStatement.setInt(2, complexId.getId2());
            }
            else {
                preparedStatement =  DataBaseUtil.getConnection().prepareStatement(query);
            }

            Integer resultSet = preparedStatement.executeUpdate();
            return resultSet;
        } catch (SQLException ex) {
            System.err.println("Error in BookingOperation.delete() \n" + ex.toString());
        }
        return 0;
    }
}
