package com.service.api.entity;

/**
 * Created by b1nt_ on 23.12.2016.
 */
public class WorkroomInventoryEntity {
    private Integer workroom_id;
    private Integer equipment_id;

    public WorkroomInventoryEntity(Integer workroom_id,
                                   Integer equipment_id) {
        this.workroom_id = workroom_id;
        this.equipment_id = equipment_id;
    }

    public Integer getWorkroom_id() {
        return workroom_id;
    }

    public void setWorkroom_id(Integer workroom_id) {
        this.workroom_id = workroom_id;
    }

    public Integer getEquipment_id() {
        return equipment_id;
    }

    public void setEquipment_id(Integer equipment_id) {
        this.equipment_id = equipment_id;
    }

    @Override
    public String toString() {
        return "WorkroomInventoryEntity{" +
                "workroom_id=" + workroom_id +
                ", equipment_id=" + equipment_id +
                '}';
    }
}
