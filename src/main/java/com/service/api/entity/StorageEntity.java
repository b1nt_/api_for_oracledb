package com.service.api.entity;

import java.sql.Blob;

/**
 * Created by Слава on 24.12.2016.
 */
public class StorageEntity {
    private Integer sond_id;
    private Integer type;
    private Blob soundtrack;

    public StorageEntity(Integer sond_id,
                         Integer type,
                         Blob soundtrack) {
        this.sond_id = sond_id;
        this.type = type;
        this.soundtrack = soundtrack;
    }

    public StorageEntity(Integer sond_id,
                         Integer type) {
        this.sond_id = sond_id;
        this.type = type;
    }

    public Integer getSond_id() {
        return sond_id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Blob getSoundtrack() {
        return soundtrack;
    }

    public void setSoundtrack(Blob soundtrack) {
        this.soundtrack = soundtrack;
    }

    @Override
    public String toString() {
        return "StorageEntity{" +
                "sond_id=" + sond_id +
                ", type=" + type +
                ", soundtrack=" + soundtrack +
                '}';
    }
}
