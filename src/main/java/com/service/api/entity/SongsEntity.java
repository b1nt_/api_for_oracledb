package com.service.api.entity;

/**
 * Created by Слава on 24.12.2016.
 */
public class SongsEntity {
    private Integer song_id;
    private String name;

    public SongsEntity(Integer song_id,
                           String name) {
        this.song_id = song_id;
        this.name = name;
    }

    public SongsEntity(String name) {
        this.name = name;
    }

    public Integer getSong_id() {
        return song_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SongsEntity{" +
                "song_id=" + song_id +
                ", name='" + name + '\'' +
                '}';
    }
}
