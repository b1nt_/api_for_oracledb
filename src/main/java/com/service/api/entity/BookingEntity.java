package com.service.api.entity;

/**
 * Created by Слава on 24.12.2016.
 */
public class BookingEntity {
    private Integer contract_id;
    private Integer department_id;

    public BookingEntity(Integer contract_id,
                                   Integer department_id) {
        this.contract_id = contract_id;
        this.department_id = department_id;
    }

    public Integer getContract_id() {
        return contract_id;
    }

    public void setContract_id(Integer contract_id) {
        this.contract_id = contract_id;
    }

    public Integer getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(Integer department_id) {
        this.department_id = department_id;
    }

    @Override
    public String toString() {
        return "BookingEntity{" +
                "contract_id=" + contract_id +
                ", department_id=" + department_id +
                '}';
    }
}
