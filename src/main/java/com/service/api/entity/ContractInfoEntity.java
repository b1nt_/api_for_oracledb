package com.service.api.entity;

/**
 * Created by Слава on 24.12.2016.
 */
public class ContractInfoEntity {
    private Integer contract_id;
    private Integer person_id;

    public ContractInfoEntity(Integer contract_id,
                         Integer person_id) {
        this.contract_id = contract_id;
        this.person_id = person_id;
    }

    public Integer getContract_id() {
        return contract_id;
    }

    public void setContract_id(Integer contract_id) {
        this.contract_id = contract_id;
    }

    public Integer getPerson_id() {
        return person_id;
    }

    public void setPerson_id(Integer person_id) {
        this.person_id = person_id;
    }

    @Override
    public String toString() {
        return "ContractInfoOperation{" +
                "contract_id=" + contract_id +
                ", person_id=" + person_id +
                '}';
    }
}
