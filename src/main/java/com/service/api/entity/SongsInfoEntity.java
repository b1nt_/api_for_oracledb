package com.service.api.entity;

/**
 * Created by Слава on 24.12.2016.
 */
public class SongsInfoEntity {
    private Integer song_id;
    private Integer person_id;

    public SongsInfoEntity(Integer song_id,
                           Integer person_id) {
        this.song_id = song_id;
        this.person_id = person_id;
    }

    public Integer getSong_id() {
        return song_id;
    }

    public void setSong_id(Integer song_id) {
        this.song_id = song_id;
    }

    public Integer getPerson_id() {
        return person_id;
    }

    public void setPerson_id(Integer person_id) {
        this.person_id = person_id;
    }

    @Override
    public String toString() {
        return "SongsInfoEntity{" +
                "song_id=" + song_id +
                ", person_id=" + person_id +
                '}';
    }
}
