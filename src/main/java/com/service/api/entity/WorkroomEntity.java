package com.service.api.entity;

/**
 * Created by Слава on 20.12.2016.
 */
public class WorkroomEntity {
    private Integer workroom_id;
    private Integer room_number;
    private String name;
    private Integer studio_id;

    public WorkroomEntity(Integer workroom_id,
                          Integer room_number,
                          String name,
                          Integer studio_id) {
        this.workroom_id = workroom_id;
        this.room_number = room_number;
        this.name = name;
        this.studio_id = studio_id;
    }

    public WorkroomEntity(Integer room_number,
                          String name,
                          Integer studio_id) {
        this.room_number = room_number;
        this.name = name;
        this.studio_id = studio_id;
    }

    public Integer getWorkroom_id() {
        return workroom_id;
    }

    public Integer getRoom_number() {
        return room_number;
    }

    public void setRoom_number(Integer room_number) {
        this.room_number = room_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStudio_id() {
        return studio_id;
    }

    public void setStudio_id(Integer studio_id) {
        this.studio_id = studio_id;
    }

    @Override
    public String toString() {
        return "WorkroomEntity{" +
                "workroom_id=" + workroom_id +
                ", room_number=" + room_number +
                ", name='" + name + '\'' +
                ", studio_id=" + studio_id +
                '}';
    }
}
