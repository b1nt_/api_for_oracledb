package com.service.api.entity;

import java.sql.Date;

/**
 * Created by Слава on 23.12.2016.
 */
public class ContractsEntity {
    private Integer contract_id;
    private String name;
    private Date start_date;
    private Date end_date;
    private Float cost;
    private String description;

    public ContractsEntity(Integer contract_id,
                           String name,
                           Date start_date,
                           Date end_date,
                           Float cost,
                           String description) {
        this.contract_id = contract_id;
        this.name = name;
        this.start_date = start_date;
        this.end_date = end_date;
        this.cost = cost;
        this.description = description;
    }

    public ContractsEntity(String name,
                           Date start_date,
                           Date end_date,
                           Float cost,
                           String description) {
        this.name = name;
        this.start_date = start_date;
        this.end_date = end_date;
        this.cost = cost;
        this.description = description;
    }

    public Integer getContract_id() {
        return contract_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ContractsEntity{" +
                "contract_id=" + contract_id +
                ", name='" + name + '\'' +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                ", cost=" + cost +
                ", description='" + description + '\'' +
                '}';
    }
}
