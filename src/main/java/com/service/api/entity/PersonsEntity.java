package com.service.api.entity;

import com.service.api.customtypes.Person;

/**
 * Created by Слава on 24.12.2016.
 */
public class PersonsEntity {
    private Integer person_id;
    private Person person_ifno;
    private String profession;
    private Integer in_staff;

    public PersonsEntity(Integer person_id,
                         Person person_ifno,
                         String profession,
                         Integer in_staff) {
        this.person_id = person_id;
        this.person_ifno = person_ifno;
        this.profession = profession;
        this.in_staff = in_staff;
    }

    public PersonsEntity(Person person_ifno,
                         String profession,
                         Integer in_staff) {
        this.person_ifno = person_ifno;
        this.profession = profession;
        this.in_staff = in_staff;
    }

    public Integer getPerson_id() {
        return person_id;
    }

    public Person getPerson_ifno() {
        return person_ifno;
    }

    public void setPerson_ifno(Person person_ifno) {
        this.person_ifno = person_ifno;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Integer getIn_staff() {
        return in_staff;
    }

    public void setIn_staff(Integer in_staff) {
        this.in_staff = in_staff;
    }

    @Override
    public String toString() {
        return "PersonsEntity{" +
                "person_id=" + person_id +
                ", person_ifno=" + person_ifno +
                ", profession='" + profession + '\'' +
                ", in_staff=" + in_staff +
                '}';
    }
}
