package com.service.api.entity;

import java.sql.Date;

/**
 * Created by Слава on 24.12.2016.
 */
public class AlbumsEntity {
    private Integer album_id;
    private String name;
    private Date release_date;
    private Integer quantity;

    public AlbumsEntity(Integer album_id,
                        String name,
                        Date release_date,
                        Integer quantity) {
        this.album_id = album_id;
        this.name = name;
        this.release_date = release_date;
        this.quantity = quantity;
    }

    public AlbumsEntity(String name,
                        Date release_date,
                        Integer quantity) {
        this.name = name;
        this.release_date = release_date;
        this.quantity = quantity;
    }

    public Integer getAlbum_id() {
        return album_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRelease_date() {
        return release_date;
    }

    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "AlbumsEntity{" +
                "album_id=" + album_id +
                ", name='" + name + '\'' +
                ", release_date=" + release_date +
                ", quantity=" + quantity +
                '}';
    }
}
