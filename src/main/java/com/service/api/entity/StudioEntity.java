package com.service.api.entity;

/**
 * Created by Слава on 20.12.2016.
 */
public class StudioEntity {
    private Integer studio_id;
    private String name;
    private String address;

    public StudioEntity(Integer studio_id,
                        String name,
                        String address) {
        this.studio_id = studio_id;
        this.name = name;
        this.address = address;
    }

    public StudioEntity(String name,
                        String address) {
        this.name = name;
        this.address = address;
    }

    public Integer getStudio_id() {
        return studio_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "StudioEntity{" +
                "studio_id=" + studio_id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
