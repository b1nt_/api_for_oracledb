package com.service.api.entity;

/**
 * Created by Слава on 13.12.2016.
 */
public class DepartmentsEntity {
    private Integer department_id;
    private String name;
    private Integer studio_id;

    public DepartmentsEntity(Integer department_id,
                             String name,
                             Integer studio_id) {
        this.department_id = department_id;
        this.name = name;
        this.studio_id = studio_id;
    }

    public DepartmentsEntity(String name,
                             Integer studio_id) {
        this.name = name;
        this.studio_id = studio_id;
    }

    public Integer getDepartment_id() {
        return department_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStudio_id() {
        return studio_id;
    }

    public void setStudio_id(Integer studio_id) {
        this.studio_id = studio_id;
    }

    @Override
    public String toString() {
        return "DepartmentsEntity{" +
                "department_id=" + department_id +
                ", name='" + name + '\'' +
                ", studio_id=" + studio_id +
                '}';
    }
}
