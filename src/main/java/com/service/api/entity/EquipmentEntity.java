package com.service.api.entity;

/**
 * Created by Слава on 23.12.2016.
 */
public class EquipmentEntity {
    private Integer equipment_id;
    private String name;

    public EquipmentEntity(Integer equipment_id,
                           String name) {
        this.equipment_id = equipment_id;
        this.name = name;
    }

    public EquipmentEntity(String name) {
        this.name = name;
    }

    public Integer getEquipment_id() {
        return equipment_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "EquipmentEntity{" +
                "equipment_id=" + equipment_id +
                ", name='" + name + '\'' +
                '}';
    }
}
