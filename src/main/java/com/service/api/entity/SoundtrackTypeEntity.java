package com.service.api.entity;

/**
 * Created by Слава on 24.12.2016.
 */
public class SoundtrackTypeEntity {
    private Integer type;
    private String description;

    public SoundtrackTypeEntity(Integer type,
                           String description) {
        this.type = type;
        this.description = description;
    }

    public SoundtrackTypeEntity(String description) {
        this.description = description;
    }

    public Integer getEquipment_id() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "SoundtrackTypeEntity{" +
                "type=" + type +
                ", description='" + description + '\'' +
                '}';
    }
}
