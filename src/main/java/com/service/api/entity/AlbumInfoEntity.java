package com.service.api.entity;

/**
 * Created by Слава on 24.12.2016.
 */
public class AlbumInfoEntity {
    private Integer album_id;
    private Integer song_id;

    public AlbumInfoEntity(Integer album_id,
                           Integer song_id) {
        this.album_id = album_id;
        this.song_id = song_id;
    }

    public Integer getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(Integer album_id) {
        this.album_id = album_id;
    }

    public Integer getSong_id() {
        return song_id;
    }

    public void setSong_id(Integer song_id) {
        this.song_id = song_id;
    }

    @Override
    public String toString() {
        return "AlbumInfoEntity{" +
                "album_id=" + album_id +
                ", song_id=" + song_id +
                '}';
    }
}
