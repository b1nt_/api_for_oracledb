package com.service.api.customtypes;

import java.sql.Date;

/**
 * Created by Слава on 24.12.2016.
 */
public class Person {
    private String name;
    private String surname;
    private String middle_name;
    private Date birthday;

    public Person(String name,
                  String surname,
                  String middle_name,
                  Date birthday) {
        this.name = name;
        this.surname = surname;
        this.middle_name = middle_name;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middle_name='" + middle_name + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
