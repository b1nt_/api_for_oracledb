package com.service.api.customtypes;

/**
 * Created by b1nt_ on 23.12.2016.
 */
public class ComplexId {
    private Integer id1;
    private Integer id2;

    public ComplexId(Integer id1,
                     Integer id2) {
        this.id1 = id1;
        this.id2 = id2;
    }

    public Integer getId1() {
        return id1;
    }

    public void setId1(Integer id1) {
        this.id1 = id1;
    }

    public Integer getId2() {
        return id2;
    }

    public void setId2(Integer id2) {
        this.id2 = id2;
    }

    @Override
    public String toString() {
        return "ComplexId{" +
                "id1=" + id1 +
                ", id2=" + id2 +
                '}';
    }
}
