package com.service;

import com.service.useoperations.*;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Слава on 13.12.2016.
 */
public class Main {
    private static String operation;
    private static String tableName;
    private static String[] params;

    public static void main(String[] args) {
        DataBaseUtil dataBaseUtil = new DataBaseUtil();
        Connection connection = dataBaseUtil.createConnection();
        System.out.println("Connected to database success.");

        parseArgs(args);

        UseTable useTable = changeTable(tableName);
        changeOperation(operation, useTable);

        try {
            if(!connection.isClosed()) {
                connection.close();
            }
            System.out.println("Close connection to database.");
        } catch (SQLException ex) {
            System.err.println("Error in connection.close().\n" + ex);
        }
    }

    static void parseArgs (String[] args) {
        operation = args.length > 0 ? args[0].toLowerCase() : "Empty operation";
        tableName = args.length > 1 ? args[1].toLowerCase() : "Empty tableName";
        params = args.length > 2 ? new String[args.length - 2] : new String [0];

        if(args.length > 2) {
            System.arraycopy(args, 2, params, 0, params.length);
        }
    }

    static UseTable changeTable(String tableName) {
        UseTable useTable = null;
        switch (tableName){
            case "departments":
                useTable = new UseDepartments();
                break;
            case "studio":
                useTable = new UseStudio();
                break;
            case "workroom":
                useTable = new UseWorkroom();
                break;
            case "workroominventory":
                useTable = new UseWorkroomInventory();
                break;
            case "equipment":
                useTable = new UseEquipment();
                break;
            case "contracts":
                useTable = new UseContracts();
                break;
            case "booking":
                useTable = new UseBooking();
                break;
            case "contractinfo":
                useTable = new UseContractInfo();
                break;
            case "persons":
                useTable = new UsePersons();
                break;
            case "songsinfo":
                useTable = new UseSongsInfo();
                break;
            case "songs":
                useTable = new UseSongs();
                break;
            case "albuminfo":
                useTable = new UseAlbumInfo();
                break;
            case "albums":
                useTable = new UseAlbums();
                break;
            case "storage":
                useTable = new UseStorage();
                break;
            case "soundtracktype":
                useTable = new UseSoundtrackType();
                break;
            default:
                System.err.println("Error! Unknown table name: " + tableName);
                System.exit(1);
        }
        return useTable;
    }

    static void changeOperation(String operation, UseTable useTable) {
        switch (operation) {
            case "create" :
                useTable.useCreate(params);
                break;
            case "read":
                useTable.useRead(params);
                break;
            case "update":
                useTable.useUpdate(params);
                break;
            case "delete":
                useTable.useDelete( params);
                break;
            default: System.err.println("Error! Unknown command: " + operation);
        }
    }
}